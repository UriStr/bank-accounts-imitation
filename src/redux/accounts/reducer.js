import {
	LOAD_ACCOUNTS,
	LOAD_ACCOUNTS_FAILURE,
	LOAD_ACCOUNTS_SUCCESS,
	CHANGE_ACCOUNT_TITLE,
	ADD_ACCOUNT,
	REMOVE_EXTERNAL_ACCOUNT,
} from './actionTypes';

export default (state = [], action) => {
	switch (action.type) {
		case LOAD_ACCOUNTS_SUCCESS:
			return action.payload;

		case LOAD_ACCOUNTS:
			return null;

		case LOAD_ACCOUNTS_FAILURE:
			return null;

		case CHANGE_ACCOUNT_TITLE:
			return state.map(item => {
				if (item.id === action.payload.id) {
					return { ...item, customTitle: action.payload.customTitle };
				}
				return item;
			});

		case ADD_ACCOUNT:
			return [...state, action.payload];

		case REMOVE_EXTERNAL_ACCOUNT:
			return state.filter(
				item => !(item.id === action.payload.id && item.type === 'external')
			);

		default:
			return state;
	}
};
