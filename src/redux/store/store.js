import { createStore, applyMiddleware } from 'redux';
import combinedReducer from '../rootReducer';
import thunk from 'redux-thunk';

const preloadedState = {
	accounts: [],
	operations: [],
};

const store = createStore(
	combinedReducer,
	preloadedState,
	applyMiddleware(thunk)
);

export default store;
