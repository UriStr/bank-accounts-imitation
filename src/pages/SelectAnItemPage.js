import React from 'react';
import withTitle from '../decorators/withTitle';

const SelectAnItemPage = () => <h2>Выберите пункт из списка меню</h2>;

export default withTitle(() => 'Выберите пункт')(SelectAnItemPage);
