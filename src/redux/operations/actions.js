import {
	LOAD_OPERATIONS,
	LOAD_OPERATIONS_FAILURE,
	LOAD_OPERATIONS_SUCCESS,
} from './actionTypes';

import { getOperations } from '../../services/requestMock';

export const loadOperationsSuccess = payload => ({
	type: LOAD_OPERATIONS_SUCCESS,
	payload,
});

export const loadOperationsFailureAction = {
	type: LOAD_OPERATIONS_FAILURE,
};

export const loadOperationsAction = {
	type: LOAD_OPERATIONS,
};

export const loadOperations = id => {
	return async dispatch => {
		dispatch(loadOperationsAction);

		await getOperations(id)
			.then(result => dispatch(loadOperationsSuccess(result)))
			.catch(() => dispatch(loadOperationsFailureAction));
	};
};
